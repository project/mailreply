<?php

/**
 * @file
 * Hooks provided by the mailreply module.
 */

/**
 * This hook allows to define reply types.
 *
 * A reply type defines how a reply-able e-mail notification should be built for
 * an entity type.
 *
 * The typical use-case is to send a reply-able e-mail notification when a new
 * entity is created. The implementing module enables the reply functionality
 * for a specific entity type by calling the mailreply_enable_for() function
 * which alters the e-mail notification with some additional information.
 * @see mailreply_enable_for()
 *
 * Once an e-mail notification is replied by e-mail, it is imported through
 * Mailhandler and Feeds which:
 * - filter the mailbox (Mailhandler filter plugin);
 * - authenticate the e-mail author and associate the reply to a replied entity
 *   (Mailhandler authenticate plugin);
 * - parse the reply (Mailhandler commands plugin);
 * - authorize and process the reply (Feeds processor plugin).
 *
 * In order to authenticate e-mail replies, an authentication token is generated
 * for each e-mail notification. An authentication token is composed of:
 * - a token (random sequence of characters);
 * - the user for which the token is generated;
 * - the entity which can be replied by the user;
 * - the reply type;
 * - an usage count;
 * - an error count;
 * - created/updated timestamps.
 * Depending on the settings, the authentication token expires at some point.
 *
 * By making use of subaddressing, the authentication token is included as part
 * of the 'Reply-To' mail header. The resulting address would for example
 * be reply+[token]@example.com. The subaddressing separator (+) can be
 * customized through settings.
 * @see https://tools.ietf.org/html/rfc5233
 * @see MailreplyAuthenticateAuthtoken
 *
 * In case subaddressing is disabled, the authentication token is included in a
 * message identifier as part of the 'References' and 'In-Reply-To' mail
 * headers.
 *
 * Before generating an authentication token, the reply type access callback is
 * checked to verify the recipient has the permission to reply by e-mail.
 * See 'access callback'.
 * @see callback_mailreply_type_access()
 *
 * The generated authentication tokens can include a prefix which can be used to
 * filter the messages fetched by Mailhandler while checking a mailbox.
 * See 'authtoken prefix'.
 * @see callback_mailreply_type_authtoken_prefix()
 * @see MailreplyFiltersAuthtoken
 *
 * While generating an e-mail notification, a text delimiter is prepended to it
 * in order to define the boundaries of the reply message.
 * See 'delimiter callback'.
 * @see callback_mailreply_type_delimiter()
 * @see MailreplyCommandsReplyBody
 *
 * Threading support by mail clients is achieved by providing some additional
 * mail headers.
 * See 'message-id callback' and 'references callback'.
 * @see callback_mailreply_type_message_id()
 * @see callback_mailreply_type_references()
 * Note that some mail clients (e.g. Gmail) require threaded e-mails to share
 * the same subject (may be prepended by 'Re: ').
 *
 * @return array
 *   An array keyed by reply type machine name, each value is an array
 *   containing the following elements:
 *   - 'entity type': The machine name of the entity type for which the
 *     reply type applies.
 *   - 'label': (optional) The label of the reply type to appear in
 *     administration pages.
 *   - 'access callback': The name of a function implementing
 *     callback_mailreply_type_access().
 *   - 'authtoken prefix': (optional) The prefix of the authentication tokens
 *     generated for the reply type.
 *   - 'authtoken settings': (optional) The settings for the authentication
 *     tokens generated for the reply type. See hook_authtoken_type_info().
 *   - 'delimiter callback': (optional) The name of a function implementing
 *     callback_mailreply_type_delimiter().
 *   - 'message-id callback': (optional) The name of a function implementing
 *     callback_mailreply_type_message_id().
 *   - 'references callback': (optional) The name of a function implementing
 *     callback_mailreply_type_references().
 *
 * @see mailreply_privatemsg_mailreply_type_info()
 * @see hook_mailreply_type_info_alter()
 */
function hook_mailreply_type_info() {
  return array(
    'my_reply_type' => array(
      'entity type' => 'my_entity_type',
      'label' => 'My reply type',
      'authtoken prefix' => 'MY',
      'delimiter callback' => 'my_module_reply_type_delimiter',
      'message-id callback' => 'my_module_reply_type_build_message_id',
      'references callback' => 'my_module_reply_type_build_references',
    ),
  );
}

/**
 * This hook allows to alter the reply types defined by other modules.
 *
 * @param array $types_info
 *   An alterable array representing the reply types.
 *
 * @see hook_mailreply_type_info()
 */
function hook_mailreply_type_info_alter(&$types_info) {
  $types_info['my_reply_type']['message-id callback'] = 'my_reply_build_another_message_id';
}

/**
 * This callback defines if an account can reply by e-mail to an entity.
 *
 * @param object $account
 *   The recipient of the e-mail notification.
 * @param string $entity_type
 *   The entity type associated to the e-mail notification.
 * @param object $entity
 *   The entity associated to the e-mail notification.
 *
 * @return bool
 *   Whether the access is allowed.
 *
 * @see hook_mailreply_type_info()
 */
function callback_mailreply_type_access($account, $entity_type, $entity) {
  return user_access('reply by e-mail to my_entity_type', $account);
}

/**
 * This callback defines the text delimiter used to parse a message body.
 *
 * The delimiter supports tokens provided through the authentication token.
 *
 * @param string $entity_type
 *   The entity type associated to the e-mail notification.
 *
 * @return array
 *   An array of strings keyed by language code.
 *   The default is keyed by the LANGUAGE_NONE constant.
 *
 * @see mailreply_token_info()
 * @see hook_mailreply_type_info()
 */
function callback_mailreply_type_delimiter($entity_type) {
  return array(
    LANGUAGE_NONE => 'You can reply to this my_entity_type by e-mail until [mailreply:expire].',
  );
}

/**
 * This callback defines how a 'Message-ID' mail header should be constructed.
 *
 * Its value must be unique.
 *
 * @see https://tools.ietf.org/html/rfc2822
 *
 * @param object $account
 *   The recipient of the e-mail notification.
 * @param string $entity_type
 *   The entity type associated to the e-mail notification.
 * @param object $entity
 *   The entity associated to the e-mail notification.
 *
 * @return string
 *   A string representing the unique message identifier surrounded by
 *   angle bracket characters.
 *
 * @see hook_mailreply_type_info()
 */
function callback_mailreply_type_message_id($account, $entity_type, $entity) {
  $local_part = array(
    'my_reply_type_prefix',
    $entity->created,
    $account->uid,
    $entity->nid,
  );

  return mailreply_build_message_id($local_part);
}

/**
 * This callback defines the parents Message-ID of an entity.
 *
 * The values are in the same format as returned by the
 * callback_mailreply_type_message_id() callback and will be used to build the
 * 'References' and 'In-Reply-To' mail headers.
 *
 * These mail headers permit the e-mail notifications to be threaded in
 * supporting mail clients.
 *
 * @see https://tools.ietf.org/html/rfc2822
 *
 * @param object $account
 *   The recipient of the e-mail notification.
 * @param string $entity_type
 *   The entity type associated to the e-mail notification.
 * @param object $entity
 *   The entity associated to the e-mail notification.
 *
 * @return array
 *   An array of maximum length MAILREPLY_REFERENCES_MAX_AMOUNT representing
 *   the Message-ID of the parent entities sorted in a time ascending order.
 *   The most recent one being the last. The original thread message should be
 *   included as the first value. The last value is used as the 'In-Reply-To'
 *   mail header.
 *
 * @see hook_mailreply_type_info()
 */
function callback_mailreply_type_references($account, $entity_type, $entity) {
  $parent_entities = my_module_load_parent_entities($entity_type, $entity, $limit = MAILREPLY_REFERENCES_MAX_AMOUNT, $include_original = TRUE);

  $references = array();
  foreach ($parent_entities as $parent_entity) {
    $references[] = my_module_reply_type_build_message_id($account, $entity_type, $parent_entity);
  }

  return $references;
}

/**
 * This hook allows to alter the delimiter by reply type.
 *
 * @param array $delimiter
 *   An alterable array.
 * @param string $reply_type
 *   The reply type associated to the delimiter or NULL for the default
 *   delimiter.
 *
 * @see callback_mailreply_type_delimiter()
 */
function hook_mailreply_delimiter_alter(&$delimiter, $reply_type = NULL) {
  $delimiter['another_reply_type'] = array(
    LANGUAGE_NONE => 'You can reply to this another_entity_type by e-mail until [mailreply:expire].',
  );
}

/**
 * This hook allows to alter the result of the reply body parser.
 *
 * @param string $body
 *   The alterable message body.
 * @param array $message
 *   The message being processed, see MailhandlerParser::parse().
 *
 * @see MailreplyCommandsReplyBody
 */
function hook_mailreply_body_parse_post_process_alter(&$body, $message) {
  if (!empty($body)) {
    $body = "Replied by e-mail:\n" . $body;
  }
}
