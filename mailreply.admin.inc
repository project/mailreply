<?php

/**
 * @file
 * Administration pages for the mailreply module.
 */

/**
 * Build the settings form.
 */
function mailreply_settings_form() {
  $form = array();

  $form['mailreply_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Reply-To e-mail address'),
    '#default_value' => variable_get('mailreply_address', ''),
    '#description' => t('This e-mail address will be used as the Reply-To address for sent notifications. A <a href="@url">Mailhandler mailbox</a> must be configured in order to import the replies sent to this address.<br />Leave blank to disable sending reply-able notifications. <em>Note that replies to previously sent e-mails may still be processed if their associated authentication token is not expired.</em>', array('@url' => url('admin/structure/mailhandler'))),
    '#element_validate' => array('mailreply_element_validate_email_address'),
  );

  $form['mailreply_subaddressing_separator'] = array(
    '#type' => 'textfield',
    '#title' => t('Reply-To e-mail subaddressing separator'),
    '#default_value' => variable_get('mailreply_subaddressing_separator', MAILREPLY_SUBADDRESSING_SEPARATOR),
    '#description' => t('When generating reply addresses, this will be used as the separator between the common local-part and the authentication token. Leave blank to disable subaddressing, in which case the authentication token will be included in the References mail header.'),
    '#element_validate' => array('mailreply_element_validate_subaddressing_separator'),
  );

  $form['mailreply_authtoken_max_usage_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Max usage count'),
    '#default_value' => variable_get('mailreply_authtoken_max_usage_count', MAILREPLY_AUTHTOKEN_MAX_USAGE_COUNT),
    '#description' => t('Limit the number of times an authentication token can be used, or put in another way: how many times an e-mail notification can be replied.<br />Set to 0 for no limit.'),
    '#element_validate' => array('mailreply_element_validate_integer_positive_or_zero'),
    '#required' => TRUE,
  );

  $form['mailreply_authtoken_max_error_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Max error count'),
    '#default_value' => variable_get('mailreply_authtoken_max_error_count', MAILREPLY_AUTHTOKEN_MAX_ERROR_COUNT),
    '#description' => t('Limit the number of times authentication token errors are processed. In practice, how many times e-mail notification errors should be sent back to the author of a message in case the authentication token failed validation (max usage, expired, invalid/blocked account).'),
    '#element_validate' => array('mailreply_element_validate_integer_positive_or_zero'),
    '#required' => TRUE,
  );

  $form['mailreply_body_delimiter_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Text delimiter'),
    '#description' => t('Included at the top of reply-able e-mail notifications, the delimiter is used to parse the reply.'),
    '#default_value' => variable_get('mailreply_body_delimiter_text', MAILREPLY_BODY_DELIMITER_TEXT),
    '#element_validate' => array('mailreply_element_validate_delimiter'),
    '#required' => TRUE,
  );

  if (module_exists('token')) {
    $token_options = array(
      'token_types' => array('mailreply'),
      'dialog' => TRUE,
      'global_types' => FALSE, // not enforced
    );
    $token_tree = theme('token_tree', $token_options);
    $form['mailreply_body_delimiter_text']['#description'] .= ' ' . $token_tree;
  }
  else {
    $form['mailreply_body_delimiter_text']['#description'] .= ' ' . t('Note: install the <a href="@url">Token module</a> to view the available tokens.', array('@url' => 'https://www.drupal.org/project/token'));
  }

  $default_domain = mailreply_get_message_id_default_domain();
  $form['mailreply_message_id_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Message-ID domain'),
    '#default_value' => variable_get('mailreply_message_id_domain', $default_domain),
    '#description' => t('The domain used for generating the Message-ID of sent reply-able e-mails.'),
    '#required' => TRUE,
  );

  $form['mailreply_authtoken_threshold_expire'] = array(
    '#type' => 'textfield',
    '#title' => t('Authentication token expire threshold (seconds)'),
    '#default_value' => variable_get('mailreply_authtoken_threshold_expire', MAILREPLY_AUTHTOKEN_THRESHOLD_EXPIRE),
    '#description' => t('The amount of time after which authentication tokens are expired.'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#required' => TRUE,
  );

  $form['mailreply_authtoken_threshold_delete'] = array(
    '#type' => 'textfield',
    '#title' => t('Authentication token delete threshold (seconds)'),
    '#default_value' => variable_get('mailreply_authtoken_threshold_delete', MAILREPLY_AUTHTOKEN_THRESHOLD_DELETE),
    '#description' => t('The amount of time after which authentication tokens are deleted.'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#required' => TRUE,
  );

  $form['#validate'][] = 'mailreply_settings_form_validate';
  $form['#submit'][] = 'mailreply_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Validate callback for an e-mail address form element.
 */
function mailreply_element_validate_email_address($element, &$form_state) {
  $value = trim($element['#value']);
  form_set_value($element, $value, $form_state);
  if (!empty($value) && !valid_email_address($value)) {
    form_error($element, t('%name field is not valid.', array('%name' => $element['#title'])));
  }
}

/**
 * Validate callback for a subaddressing separator form element.
 */
function mailreply_element_validate_subaddressing_separator($element, &$form_state) {
  $value = $element['#value'];
  if (!empty($value) &&
      !valid_email_address('test' . $value . mailreply_authtoken_generate_token() . '@example.com')) {
    form_error($element, t('%name field is not valid.', array('%name' => $element['#title'])));
  }
}

/**
 * Validate callback for a text delimiter form element.
 */
function mailreply_element_validate_delimiter($element, &$form_state) {
  $value = $element['#value'];
  // @see drupal_wrap_mail()
  $max_length = 77;
  if (!empty($value)) {
    $test = str_replace('mailreply:expire', 'current-date', $value);
    $length = strlen(token_replace($test));
    if ($length > $max_length) {
      form_error($element, t('%name, including replaced tokens, cannot be longer than %max characters but is currently %length characters long.', array('%name' => $element['#title'], '%max' => $max_length, '%length' => $length)));
    }
  }
}

/**
 * Validate callback for a form element that must be a positive integer or zero.
 *
 * @see element_validate_integer_positive()
 */
function mailreply_element_validate_integer_positive_or_zero($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
    form_error($element, t('%name field must be a positive integer or zero.', array('%name' => $element['#title'])));
  }
}

/**
 * Form validate callback for mailreply_settings_form().
 */
function mailreply_settings_form_validate($form, &$form_state) {
  // Message-ID domain.
  form_set_value($form['mailreply_message_id_domain'], trim($form_state['values']['mailreply_message_id_domain']), $form_state);

  // Thresholds.
  if ($form_state['values']['mailreply_authtoken_threshold_delete'] < $form_state['values']['mailreply_authtoken_threshold_expire']) {
    form_set_error('mailreply_authtoken_threshold_delete', t('Authentication token delete threshold must be greater or equal to the Authentication token expire threshold.'));
  }
}

/**
 * Form submit callback for mailreply_settings_form().
 */
function mailreply_settings_form_submit($form, &$form_state) {
  mailreply_info_cache_clear();
  cache_clear_all('mailreply_default_delimiter', 'cache');
}

/**
 * Page callback for the invalidate authentication tokens by user role form.
 */
function mailreply_authtoken_invalidate_by_user_role_page() {
  if ($mailreply_types = mailreply_get_info()) {
    $authtoken_types = array_keys($mailreply_types);

    return authtoken_get_invalidate_by_user_role_form(
      $authtoken_types,
      'admin/config/system/mailreply/invalidate'
    );
  }

  drupal_set_message(t('A mailreply type module must be enabled to be able to invalidate authentication tokens by user role.'), 'error');
  drupal_goto('admin/config/system/mailreply');
}
