<?php

/**
 * @file
 * MailreplyFiltersAuthtoken class.
 */

/**
 * Filters on the authentication token prefix of reply types.
 */
abstract class MailreplyFiltersAuthtoken extends MailhandlerFilters {

  /**
   * Reply types to filter.
   *
   * @return array
   *   An array of reply types machine name.
   */
  abstract protected function replyTypes();

  /**
   * Filter.
   */
  public function fetch($header) {
    // Reply address.
    $reply_address = variable_get('mailreply_address');
    if (empty($reply_address)) {
      return FALSE;
    }

    // Reply types.
    $reply_types = $this->replyTypes();
    if (empty($reply_types)) {
      return FALSE;
    }

    // Authtoken prefixes.
    $prefixes = array();
    foreach ($reply_types as $reply_type) {
      if ($prefix = mailreply_type_authtoken_prefix($reply_type)) {
        $prefixes[] = $prefix;
      }
    }
    if (empty($prefixes)) {
      return FALSE;
    }

    $sep = variable_get('mailreply_subaddressing_separator', MAILREPLY_SUBADDRESSING_SEPARATOR);
    $subaddressing = !empty($sep);

    $pattern = "/" .
      preg_quote($sep, '/') .
      mailreply_authtoken_build_token_pattern($prefixes, '/') .
      "@/i";

    // Subaddressing.
    if ($subaddressing) {
      $fields = array('toaddress', 'ccaddress', 'bccaddress');
      $recipients = array();
      foreach ($fields as $field) {
        if (isset($header->{$field})) {
          $recipients[] = $header->{$field};
        }
      }

      return preg_match($pattern, implode(" ", $recipients));
    }

    // References.
    return (isset($header->references) && preg_match($pattern, $header->references));
  }

}
