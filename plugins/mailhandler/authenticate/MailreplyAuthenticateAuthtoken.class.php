<?php

/**
 * @file
 * MailreplyAuthenticateAuthtoken class.
 */

/**
 * Authenticate message with authentication token.
 *
 * An authentication token is searched in the message through:
 * - subaddressing;
 * - 'References' mail header.
 *
 * An implementation of this abstract class defines for which reply types
 * issued authentication token should be processed.
 */
abstract class MailreplyAuthenticateAuthtoken extends MailhandlerAuthenticate {

  /**
   * Reply types to authenticate.
   *
   * @return array
   *   An array of reply types machine name.
   */
  abstract protected function replyTypes();

  /**
   * Authenticate a message.
   */
  public function authenticate(&$message, $mailbox) {
    $reply_types = $this->replyTypes();
    if (empty($reply_types)) {
      return FALSE;
    }

    $token = $this->getToken($message);

    if (empty($token)) {
      return 0;
    }

    // Find the authentication token.
    if ($authtoken = authtoken_load($token, $reply_types)) {

      // Validate the authentication token.
      $error = NULL;
      if (!authtoken_validate($authtoken, $error)) {
        // Reached max error count, do not process further.
        if ($error == AUTHTOKEN_INVALID_MAX_ERROR) {
          return 0;
        }

        // Increment usage and error counts.
        authtoken_inc_usage($authtoken, TRUE);

        // @see hook_mailhandler_auth_failed()
        // @see mailreply_get_auth_failed_authtoken()
        $message['mailreply_auth_failed_authtoken'] = array(
          'authtoken' => $authtoken,
          'error' => $error,
        );

        return 0;
      }

      // Keep the authentication token available for further processing.
      // @see mailreply_get_authenticated_authtoken()
      $message['mailreply_authenticated_authtoken'] = $authtoken;

      // Increment usage count.
      authtoken_inc_usage($authtoken);

      // The authenticated_uid is set to this returned value.
      return $authtoken->uid();
    }

    return 0;
  }

  /**
   * Get the token.
   *
   * @param array $message
   *   The message.
   *
   * @return string|false
   *   The token if it is found or FALSE.
   */
  public function getToken($message) {
    // Verify the Reply-To e-mail address is enabled.
    $reply_address = variable_get('mailreply_address');
    if (empty($reply_address)) {
      return FALSE;
    }
    $sep = variable_get('mailreply_subaddressing_separator', MAILREPLY_SUBADDRESSING_SEPARATOR);
    $subaddressing = !empty($sep);

    // Loop detection, check whether the message 'Reply-To' mail header is set
    // to the configured Reply-To e-mail address.
    if (isset($message['header']->reply_to[0])) {
      $sub_pattern = NULL;
      if (!empty($sep)) {
        $sub_pattern = "(" . preg_quote($sep, '/') . "[^@]*)?";
      }
      $reply_to = mailreply_find_email_address($message['header']->reply_to, $reply_address, $sub_pattern, '/');

      if (!empty($reply_to)) {
        return FALSE;
      }
    }

    // Authentication token prefixes.
    $reply_types = $this->replyTypes();
    $prefixes = array();
    foreach ($reply_types as $reply_type) {
      if ($prefix = mailreply_type_authtoken_prefix($reply_type)) {
        $prefixes[] = $prefix;
      }
    }

    // Validate at least one recipient match the reply address, according to
    // subaddressing.
    $fields = array('to', 'cc', 'bcc');
    $recipients = array();
    foreach ($fields as $field) {
      if (isset($message['header']->{$field}) && is_array($message['header']->{$field})) {
        $recipients += $message['header']->{$field};
      }
    }

    $sub_pattern = NULL;
    if ($subaddressing) {
      $sub_pattern = preg_quote($sep, '/') . mailreply_authtoken_build_token_pattern($prefixes, '/');
    }
    $recipient = mailreply_find_email_address($recipients, $reply_address, $sub_pattern, '/');
    if (empty($recipient)) {
      return FALSE;
    }

    // Search for an authentication token through subaddressing.
    if ($subaddressing) {
      return mailreply_extract_email_subaddress($recipient, $sep);
    }

    // Search for a message identifier including an authentication token
    // in the 'References' mail header.
    if (isset($message['header']->references)) {
      return mailreply_extract_references_token($message['header']->references, $reply_address, $prefixes);
    }

    return FALSE;
  }

}
