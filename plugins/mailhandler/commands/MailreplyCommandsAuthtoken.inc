<?php

/**
 * @file
 * MailreplyCommandsAuthtoken class.
 */

$plugin = array(
  'name' => 'Authentication token mapping',
  'description' => 'Provide authentication token mapping sources.',
  'handler' => array(
    'class' => 'MailreplyCommandsAuthtoken',
    'parent' => 'MailhandlerCommands',
  ),
  'file' => 'MailreplyCommandsAuthtoken.class.php',
);
