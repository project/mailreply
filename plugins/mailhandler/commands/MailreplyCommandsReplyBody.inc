<?php

/**
 * @file
 * MailreplyCommandsReplyBody class.
 */

$plugin = array(
  'name' => 'Reply body parser',
  'description' => 'Provide the reply body as a mapping source.',
  'handler' => array(
    'class' => 'MailreplyCommandsReplyBody',
    'parent' => 'MailhandlerCommands',
  ),
  'file' => 'MailreplyCommandsReplyBody.class.php',
);
