<?php

/**
 * @file
 * MailreplyCommandsAuthtoken class.
 */

/**
 * Provides mapping sources related to the authentication token.
 *
 * This plugin assumes the target entity type imported by the processor is the
 * same as the entity type associated to the authentication token.
 *
 * For other cases, for example a processor which can be mapped with sources of
 * multiple entity types, the hook_feeds_parser_sources_alter() hook or another
 * Mailhandler commands plugin must be implemented.
 */
class MailreplyCommandsAuthtoken extends MailhandlerCommands {

  /**
   * Set sources.
   */
  public function process(&$message, $source) {
    if ($authtoken = mailreply_get_authenticated_authtoken($message)) {
      $target_entity_type = $source->importer->processor->entityType();

      $message['authtoken_entity_type'] = $authtoken->entity_type();

      if ($authtoken->entity_type() == $target_entity_type) {
        $message['authtoken_entity_id'] = $authtoken->entity_id();
      }
    }
  }

  /**
   * Return mapping sources.
   */
  public function getMappingSources($config) {
    $sources = array();

    $sources['authtoken_entity_type'] = array(
      'name' => 'Entity type',
      'description' => 'Entity type associated to the authentication token.',
    );

    $sources['authtoken_entity_id'] = array(
      'name' => 'Entity id',
      'description' => 'Entity id associated to the authentication token.',
    );

    return $sources;
  }

}
