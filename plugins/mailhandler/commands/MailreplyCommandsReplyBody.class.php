<?php

/**
 * @file
 * MailreplyCommandsReplyBody class.
 */

/**
 * Basic parser to get the reply content above a text delimiter.
 *
 * It tries to strip reply headers like 'On [date], [author] wrote:'.
 *
 * Inspirations:
 * - Mail Comment module filters;
 * - Discourse reply trimmer.
 */
class MailreplyCommandsReplyBody extends MailhandlerCommands {

  const MAPPING_SOURCE_TEXT = 'reply_body_text';

  protected $authtoken;

  /**
   * Parse the reply body.
   */
  public function process(&$message, $source) {
    // Require the source to be mapped.
    if (!$this->isMapped($source, self::MAPPING_SOURCE_TEXT)) {
      return;
    }

    if ($authtoken = mailreply_get_authenticated_authtoken($message)) {
      $this->authtoken = $authtoken;
    }

    $body = $message['body_text'];

    $body = $this->parseReply($body);

    // Post-process hook.
    drupal_alter('mailreply_body_parse_post_process', $body, $message);

    $message[self::MAPPING_SOURCE_TEXT] = $body;
  }

  /**
   * Parse the reply.
   */
  protected function parseReply($body) {
    $delimiters = $this->getDelimiters();
    $body = $this->extractUntilDelimiter($body, $delimiters);

    $patterns = $this->getPatterns();
    $body = $this->extractUntilPatterns($body, $patterns);

    // In some cases, body_text is the same as body_html.
    // @see MailhandlerPhpImapRetrieve::retrieve_message()
    $body = drupal_html_to_text($body);

    $body = trim($body);

    return $body;
  }

  /**
   * Get delimiters.
   */
  protected function getDelimiters() {
    $reply_type = NULL;
    if (!empty($this->authtoken)) {
      $reply_type = $this->authtoken->type();
    }

    // Search for the delimiter in enabled languages.
    $delimiters = mailreply_type_delimiter($reply_type);

    return $delimiters;
  }

  /**
   * Get delimiter patterns as regular expressions.
   */
  protected function getPatterns() {
    $patterns = array(
      // signature
      '/(^|\R+)-- \R/',

      // -- Original Message --
      '/(^|\R+)[_-]{2,}\s*(?:Original|Reply) Message\s*[_-]{2,}\s*/is',
      // __________ (Outlook.com)
      '/(^|\R+)(?:[_-]{10,}|<hr[^>]+>)\s*[^\r\n]*From:\s*(?:\S|<[^>]*>)*/s',
      // "(unquoted) ... [date], [author] wrote:\n(>|quoted lines) $"
      '/(^|\R+)[^\r\n\s>]+[^\r\n]*[0-9\-\/]+, [^\r\n]* wrote:\R+(\s*>+[^\r\n]*\R*)+$/s',
      // "(unquoted) ... [email] ...:\n(>|quoted lines) $"
      '/(^|\R+)[^\r\n\s>]+[^\r\n]*\S+@\S+[^\r\n]*:\R+(\s*>+[^\r\n]*\R*)+$/s',

      // -- ... -- \n ...:
      '/(^|\R+)[_-]{2,}\s*[^\r\n]+\s*[_-]{2,}\s*\R\S+:/s',
      // __________ (Outlook.com)
      '/(^|\R+)(?:[_-]{10,}|<hr[^>]+>)\s*\R\S+:\s*(?:\S|<[^>]*>)*/s',
      // "(unquoted) ... :\n(>|quoted lines) $"
      '/(^|\R+)[^\r\n\s>]+[^\r\n]*:\R+(\s*>+[^\r\n]*\R*)+$/s',
    );

    return $patterns;
  }

  /**
   * Get a part of body until a delimiter.
   *
   * Extract a substring of body from start until the position of the first
   * delimiter match.
   *
   * @param string $body
   *   The message body.
   * @param array $delimiters
   *   An array of delimiter strings.
   *
   * @return string
   *   Returns the extracted substring of body.
   */
  protected function extractUntilDelimiter($body, array $delimiters) {
    $patterns = array();

    foreach ($delimiters as $delimiter) {
      // Delimiter may contain tokens.
      // @see token_scan()
      if ($subs = preg_split("/(\[[^\s\[\]:]*:[^\[\]]*\])/", $delimiter)) {
        // Quote substrings.
        $quoted_subs = array();
        foreach ($subs as $sub) {
          $quoted_subs[] = preg_quote($sub, '/');
        }

        // Build a regular expression where tokens are replaced by wildcards.
        $patterns[] = '/' . implode('[^\r\n]+', $quoted_subs) . '/';
      }
    }

    return $this->extractUntilPatterns($body, $patterns);
  }

  /**
   * Get a part of body until a regular expression match.
   *
   * Extract a substring of body from start until the position of the first
   * pattern match.
   *
   * The patterns are regular expressions ordered by descending priority.
   *
   * @param string $body
   *   The message body.
   * @param array $patterns
   *   An array of regular expressions.
   *
   * @return string
   *   Returns the extracted substring of body.
   */
  protected function extractUntilPatterns($body, array $patterns) {
    if (empty($patterns)) {
      return $body;
    }

    $matches = array();
    $offset = NULL;

    foreach ($patterns as $pattern) {
      preg_match($pattern, $body, $matches, PREG_OFFSET_CAPTURE);
      if (!empty($matches)) {
        $offset = $matches[0][1];
        break;
      }
    }

    if (isset($offset)) {
      $body = substr($body, 0, $offset);
    }

    return $body;
  }

  /**
   * Whether the mapping source is mapped.
   */
  protected function isMapped($source, $mapping_source) {
    $mappings = $source->importer()->processor->getMappings();

    if (empty($mappings)) {
      return FALSE;
    }

    foreach ($mappings as $mapping) {
      if ($mapping['source'] == $mapping_source) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Return mapping sources.
   */
  public function getMappingSources($config) {
    $sources = array();

    $sources[self::MAPPING_SOURCE_TEXT] = array(
      'name' => 'Reply body text',
      'description' => 'Reply body text',
    );

    return $sources;
  }

}
