<?php

/**
 * @file
 * Mailreply module tokens.
 */

/**
 * Implements hook_token_info().
 */
function mailreply_token_info() {
  $types = array(
    'mailreply' => array(
      'name' => t('Authentication token'),
      'description' => t('Tokens related to a Mailreply authentication token.'),
      'needs-data' => 'authtoken',
    ),
  );

  $tokens = array(
    'mailreply' => array(
      'expire' => array(
        'name' => t('Expiration date'),
        'description' => t('The date the authentication token expires.'),
        'type' => 'date',
      ),
    ),
  );

  return array(
    'types' => $types,
    'tokens' => $tokens,
  );
}

/**
 * Implements hook_tokens().
 */
function mailreply_tokens($type, $tokens, array $data = array(), array $options = array()) {
  if ($type != 'mailreply' || empty($data['authtoken']) || !($data['authtoken'] instanceof Authtoken)) {
    return array();
  }

  $language_code = NULL;
  if (!empty($options['language'])) {
    $language_code = $options['language']->language;
  }

  $authtoken = $data['authtoken'];
  $threshold_expire = variable_get('mailreply_authtoken_threshold_expire', MAILREPLY_AUTHTOKEN_THRESHOLD_EXPIRE);
  $authtoken_expire = $authtoken->created() + $threshold_expire;

  $replacements = array();

  foreach ($tokens as $name => $original) {
    switch ($name) {
      case 'expire':
        $replacements[$original] = format_date($authtoken_expire, 'medium', '', NULL, $language_code);
        break;
    }
  }

  if ($expire_tokens = token_find_with_prefix($tokens, 'expire')) {
    $replacements += token_generate('date', $expire_tokens, array('date' => $authtoken_expire), $options);
  }

  return $replacements;
}
