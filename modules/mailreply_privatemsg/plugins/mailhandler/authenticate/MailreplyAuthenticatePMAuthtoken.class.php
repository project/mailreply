<?php

/**
 * @file
 * MailreplyAuthenticatePMAuthtoken class.
 */

/**
 * Authenticate message with a mailreply_pm authentication token.
 */
class MailreplyAuthenticatePMAuthtoken extends MailreplyAuthenticateAuthtoken {

  /**
   * Define the reply type.
   */
  protected function replyTypes() {
    return array('mailreply_pm');
  }

}
