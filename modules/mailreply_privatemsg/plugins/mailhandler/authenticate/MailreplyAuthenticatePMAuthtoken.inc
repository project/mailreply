<?php

/**
 * @file
 * MailreplyAuthenticatePMAuthtoken class.
 */

$plugin = array(
  'name' => 'Authentication token (Private messages)',
  'description' => 'Authenticate a message through an authentication token issued for Private messages.',
  'handler' => array(
    'class' => 'MailreplyAuthenticatePMAuthtoken',
    'parent' => 'MailreplyAuthenticateAuthtoken',
  ),
  'file' => 'MailreplyAuthenticatePMAuthtoken.class.php',
);
