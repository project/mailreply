<?php

/**
 * @file
 * MailreplyFiltersPrivatemsg class.
 */

/**
 * Filter to fetch private messages.
 */
class MailreplyFiltersPrivatemsg extends MailreplyFiltersAuthtoken {

  /**
   * Define the reply type.
   */
  protected function replyTypes() {
    return array('mailreply_pm');
  }

}
