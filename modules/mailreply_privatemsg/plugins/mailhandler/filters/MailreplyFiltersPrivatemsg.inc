<?php

/**
 * @file
 * MailreplyFiltersPrivatemsg class.
 */

$plugin = array(
  'name' => 'Private messages only',
  'description' => 'Filter by the authentication token prefix of private messages.',
  'handler' => array(
    'class' => 'MailreplyFiltersPrivatemsg',
    'parent' => 'MailreplyFiltersAuthtoken',
  ),
  'file' => 'MailreplyFiltersPrivatemsg.class.php',
);
