<?php

/**
 * @file
 * Administration page for the mailreply privatemsg module.
 */

/**
 * Build the settings form.
 */
function mailreply_privatemsg_settings_form() {
  $form = array();

  $form['mailreply_privatemsg_authtoken_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Authentication token prefix'),
    '#default_value' => variable_get('mailreply_privatemsg_authtoken_prefix', MAILREPLY_PRIVATEMSG_AUTHTOKEN_PREFIX),
    '#description' => t('The prefix used to filter the mailbox processed by Mailhandler. It must only contain letters and numbers.'),
    '#element_validate' => array('mailreply_privatemsg_element_validate_prefix'),
    '#maxlength' => 3,
    '#required' => TRUE,
  );

  $form['mailreply_privatemsg_message_id_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Message-ID prefix'),
    '#default_value' => variable_get('mailreply_privatemsg_message_id_prefix', MAILREPLY_PRIVATEMSG_MESSAGE_ID_PREFIX),
    '#description' => t('The prefix used for generating the Message-ID headers related to Private messages. It must only contain letters and numbers.'),
    '#element_validate' => array('mailreply_privatemsg_element_validate_prefix'),
    '#maxlength' => 3,
    '#required' => TRUE,
  );

  $form['mailreply_privatemsg_send_errors'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send an e-mail notification to the reply author in case of processing errors.'),
    '#default_value' => variable_get('mailreply_privatemsg_send_errors', MAILREPLY_PRIVATEMSG_SEND_ERRORS),
  );

  $form['#submit'][] = 'mailreply_privatemsg_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Validate callback for a prefix form element.
 */
function mailreply_privatemsg_element_validate_prefix($element, &$form_state) {
  $value = $element['#value'];
  if (!empty($value) && !preg_match('/^[a-z0-9]+$/', $value)) {
    form_error($element, t('%name field is not valid.', array('%name' => $element['#title'])));
  }
}

/**
 * Form submit callback for mailreply_privatemsg_settings_form().
 */
function mailreply_privatemsg_settings_form_submit($form, &$form_state) {
  mailreply_info_cache_clear();
}

/**
 * Alter the variable_group_form in order to remove the subject variables.
 *
 * Implements hook_form_alter().
 */
function mailreply_privatemsg_form_variable_group_form_alter(&$form, &$form_state, $form_id) {
  if (!isset($form['#variable_group_form']) || $form['#variable_group_form'] != 'mailreply_privatemsg') {
    return;
  }

  $element_keys = array(
    'authtoken_expired',
    'authtoken_max_usage',
    'body_empty',
    'validation_error',
  );

  foreach ($element_keys as $key) {
    $element = &$form['mailreply_privatemsg_' . $key . '_[mail_part]'];
    unset($element['mailreply_privatemsg_' . $key . '_subject']);
  }
}
