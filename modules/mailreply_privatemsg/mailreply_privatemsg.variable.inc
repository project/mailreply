<?php

/**
 * @file
 * mailreply_privatemsg.variable.inc
 */

/**
 * Implements hook_variable_group_info().
 */
function mailreply_privatemsg_variable_group_info() {
  $groups = array(
    'mailreply_privatemsg' => array(
      'title' => t('Mailreply - Privatemsg'),
      'description' => t('Templates for Mailreply notifications.'),
      'access' => 'administer mailhandler',
      'path' => array('admin/config/system/mailreply/privatemsg/notifications'),
    ),
  );

  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function mailreply_privatemsg_variable_info($options) {
  $variables = array(
    'mailreply_privatemsg_authtoken_expired_[mail_part]' => array(
      'group' => 'mailreply_privatemsg',
      'type' => 'mail_text',
      'title' => t('Authentication token is expired', array(), $options),
      'default' => array(
        'subject' => '[site:name] - Error while processing your private message reply',
        'body' => "Your private message could not be processed because it was sent past the time limit.\n\n-- \n[site:name]",
      ),
      'localize' => TRUE,
      'required' => TRUE,
    ),
    'mailreply_privatemsg_authtoken_max_usage_[mail_part]' => array(
      'group' => 'mailreply_privatemsg',
      'type' => 'mail_text',
      'title' => t('Authentication token reached max usage', array(), $options),
      'default' => array(
        'subject' => '[site:name] - Error while processing your private message reply',
        'body' => "Your private message could not be processed because your reply was already processed.\n\n-- \n[site:name]",
      ),
      'localize' => TRUE,
      'required' => TRUE,
    ),
    'mailreply_privatemsg_body_empty_[mail_part]' => array(
      'group' => 'mailreply_privatemsg',
      'type' => 'mail_text',
      'title' => t('Empty reply', array(), $options),
      'default' => array(
        'subject' => '[site:name] - Error while processing your private message reply',
        'body' => "Your private message could not be processed because your reply was empty.\n\n-- \n[site:name]",
      ),
      'localize' => TRUE,
      'required' => TRUE,
    ),
    'mailreply_privatemsg_validation_error_[mail_part]' => array(
      'group' => 'mailreply_privatemsg',
      'type' => 'mail_text',
      'title' => t('Validation error', array(), $options),
      'default' => array(
        'subject' => '[site:name] - Error while processing your private message reply',
        'body' => "There was an error while processing your private message.\n\n-- \n[site:name]",
      ),
      'localize' => TRUE,
      'required' => TRUE,
    ),
  );

  return $variables;
}
