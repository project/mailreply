<?php

/**
 * @file
 * mailreply_privatemsg_default.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function mailreply_privatemsg_default_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'mailreply_privatemsg';
  $feeds_importer->config = array(
    'name' => 'Mailreply Privatemsg',
    'description' => 'Imports private messages from a Mailhandler mailbox',
    'fetcher' => array(
      'plugin_key' => 'MailhandlerFetcher',
      'config' => array(
        'filter' => 'MailreplyFiltersPrivatemsg',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'MailhandlerParser',
      'config' => array(
        'available_commands' => '',
        'authenticate_plugin' => 'MailreplyAuthenticatePMAuthtoken',
        'command_plugin' => array(
          'MailhandlerCommandsHeaders' => 'MailhandlerCommandsHeaders',
          'MailreplyCommandsAuthtoken' => 'MailreplyCommandsAuthtoken',
          'MailreplyCommandsReplyBody' => 'MailreplyCommandsReplyBody',
          'MailhandlerCommandsDefault' => 0,
          'MailhandlerCommandsFiles' => 0,
        ),
        'extended_headers' => NULL,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsPrivatemsgProcessor',
      'config' => array(
        'mappings' => array(
          0 => array(
            'source' => 'authenticated_uid',
            'target' => 'author_uid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'authtoken_entity_id',
            'target' => 'parent_mid',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'message_id',
            'target' => 'guid',
            'unique' => TRUE,
          ),
          3 => array(
            'source' => 'reply_body_text',
            'target' => 'message_body',
            'unique' => FALSE,
          ),
        ),
        'insert_new' => 1,
        'update_existing' => 0,
        'update_non_existent' => 'skip',
        'input_format' => NULL,
        'skip_hash_check' => FALSE,
        'bundle' => 'privatemsg_message',
        'insert_threads' => FALSE,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 900,
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['mailreply_privatemsg'] = $feeds_importer;

  return $export;
}
