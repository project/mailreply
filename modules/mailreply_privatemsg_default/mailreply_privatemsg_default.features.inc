<?php

/**
 * @file
 * mailreply_privatemsg_default.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mailreply_privatemsg_default_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
}
