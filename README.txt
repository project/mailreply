Mailreply
=========

This module leverages the Mailhandler and Feeds modules in order to allow your
users to reply by e-mail to notifications related to entity types.

By default, an implementation for the Privatemsg module is included as a
submodule and allows to reply to private messages by e-mail. Other entity types
may be supported by implementing the provided API.

The Mailreply Privatemsg quick-start submodule includes a pre-configured Feeds
importer.


Description
===========

When an e-mail notification is sent about a new entity (for example a new
private message), this module generates a reply address which include an
authentication token through e-mail subaddressing.

In case e-mail subaddressing is disabled, the authentication token is included
in a message identifier as part of the References and In-Reply-To mail headers.

The authentication token is associated to a recipient and an entity. Depending
on the settings, it expires at some point.

Subaddressing is basically a tag appended to the local part of an email address.
For example, e-mails sent to name+tag@example.com are delivered to
name@example.com.
By checking the tag part of the recipient e-mail address, new messages fetched
by Mailhandler can be authenticated.

Once the message is authenticated, the authentication token holds information
about whom it was assigned to and to which entity the message is a reply.

The module features are provided by multiple Mailhandler plugins:
* filter mailbox messages by authentication token prefix;
* authenticate a message by authentication token;
* provide the entity associated to an authentication token as a
  Feeds mapping source;
* reply parser: tries to strip the quoted original message from an e-mail reply
  by the use of a text delimiter included in sent e-mail notifications.
  The parsed result is available as a Feeds mapping source.


Setup
=====

This module is for Drupal 7 only, at least for now.

Mailreply dependencies:
* (required) Authtoken;
* (required) Mailhandler;
* (required) Feeds;
* (recommended) Token.

Mailreply Privatemsg dependencies:
* (required) Privatemsg;
* (required) Feeds Privatemsg Processor.

The [#2333087:https://www.drupal.org/project/mailhandler/issues/2333087]
Mailhandler patch is recommended.


Inspirations
============

This module is similar to the Mailcomment module, instead of being tied to
node/comment entity types, it provides a generic API which can be implemented
for any entity type.

It is also inspired by incoming e-mail handling features of
Discourse and Gitlab.
