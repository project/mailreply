<?php

/**
 * @file
 * Mailreply module variables.
 */

/**
 * Implements hook_variable_group_info().
 */
function mailreply_variable_group_info() {
  $groups = array();

  $groups['mailreply'] = array(
    'title' => t('Mailreply'),
    'description' => t('Mailreply'),
    'access' => 'administer mailhandler',
    'path' => array('admin/config/system/mailreply'),
  );

  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function mailreply_variable_info($options) {
  $variables = array();

  $variables['mailreply_body_delimiter_text'] = array(
    'group' => 'mailreply',
    'type' => 'string',
    'title' => t('Text delimiter', array(), $options),
    'default' => MAILREPLY_BODY_DELIMITER_TEXT,
    'description' => t('Included at the top of reply-able e-mail notifications, the delimiter is used to parse the reply.', array(), $options),
    'localize' => TRUE,
  );

  return $variables;
}
